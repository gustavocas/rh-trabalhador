FROM openjdk:11
VOLUME /tmp
ADD ./target/rh-trabalhador-0.0.1-SNAPSHOT.jar rh-trabalhador.jar
ENTRYPOINT ["java","-jar","/rh-trabalhador.jar"]
