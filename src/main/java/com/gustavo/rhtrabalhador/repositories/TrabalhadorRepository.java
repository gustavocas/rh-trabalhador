package com.gustavo.rhtrabalhador.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gustavo.rhtrabalhador.entities.Trabalhador;

public interface TrabalhadorRepository extends JpaRepository<Trabalhador, Integer>{

}
