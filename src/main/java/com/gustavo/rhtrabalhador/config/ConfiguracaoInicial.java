package com.gustavo.rhtrabalhador.config;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gustavo.rhtrabalhador.entities.Trabalhador;
import com.gustavo.rhtrabalhador.repositories.TrabalhadorRepository;

@Configuration
public class ConfiguracaoInicial {
	
	@Autowired
	private TrabalhadorRepository repositoryTrabalhador;
	
	@Bean
	public void gerarMassaDados() throws ParseException {
		
		System.out.println("Inserindo Trabalhadores");
		Trabalhador t1 = new Trabalhador(0,"Gustavo",10);
		Trabalhador t2 = new Trabalhador(0,"Jorge",20);
		Trabalhador t3 = new Trabalhador(0,"Marreta",30);
		Trabalhador t4 = new Trabalhador(0,"Dita",40);
		Trabalhador t5 = new Trabalhador(0,"Capanga",50);
		
		List<Trabalhador> lista = new ArrayList<>();
		lista.add(t1);
		lista.add(t2);
		lista.add(t3);
		lista.add(t4);
		lista.add(t5);
		
		repositoryTrabalhador.saveAll(lista);
		
		
	}
}
