package com.gustavo.rhtrabalhador.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.rhtrabalhador.entities.Trabalhador;
import com.gustavo.rhtrabalhador.repositories.TrabalhadorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value="trabalhador")
public class TrabalhadorResource {
	
	@Autowired
	private Environment env;
	
	private static Logger logger = LoggerFactory.getLogger(TrabalhadorResource.class);
	
	@Autowired
	private TrabalhadorRepository repositoryTrabalhador;
	
	@GetMapping(value = "listarTodos")
	public ResponseEntity<List<Trabalhador>> listarTodos(){
		List<Trabalhador> lista = repositoryTrabalhador.findAll();
		return ResponseEntity.ok(lista);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Trabalhador> listarPorId(@PathVariable Integer id) throws Exception{
		
		//imprima no log a porta da instancia
		logger.info("PORT = " + env.getProperty("local.server.port"));
		
		Optional<Trabalhador> obj = repositoryTrabalhador.findById(id);
		Trabalhador trab = obj.orElseThrow(() -> new Exception("Codigo invalido: " + id));
		return ResponseEntity.ok(trab);
	}

}
